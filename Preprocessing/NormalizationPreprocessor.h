﻿#pragma once

#include "IPreprocessor.h"
#include <string>
#include <vector>


namespace NDtw
{
	namespace Preprocessing
	{
		class NormalizationPreprocessor : public IPreprocessor
		{
		private:
			const double _minBoundary;
			const double _maxBoundary;

			/// <summary>
			/// Initialize to use normalization to range [0, 1]
			/// </summary>
		public:
			NormalizationPreprocessor();

			/// <summary>
			/// Initialize to use normalization to range [minBoundary, maxBoundary]
			/// </summary>
			NormalizationPreprocessor(double minBoundary, double maxBoundary);

			std::vector<double> Preprocess(std::vector<double> &data);

			virtual std::wstring ToString() override;
		};
	}
}
