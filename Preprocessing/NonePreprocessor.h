﻿#pragma once

#include "IPreprocessor.h"
#include <string>
#include <vector>


namespace NDtw
{
	namespace Preprocessing
	{
		class NonePreprocessor : public IPreprocessor
		{
		public:
			std::vector<double> Preprocess(std::vector<double> &data);

			virtual std::wstring ToString() override;
		};
	}
}
