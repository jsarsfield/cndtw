﻿#include "NonePreprocessor.h"

namespace NDtw
{
	namespace Preprocessing
	{

		std::vector<double> NonePreprocessor::Preprocess(std::vector<double> &data)
		{
			return data;
		}

		std::wstring NonePreprocessor::ToString()
		{
			return L"None";
		}
	}
}
