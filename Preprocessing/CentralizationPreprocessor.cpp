﻿#include "CentralizationPreprocessor.h"

namespace NDtw
{
	namespace Preprocessing
	{

		std::vector<double> CentralizationPreprocessor::Preprocess(std::vector<double> &data)
		{
			auto avg = Mean(data);
			std::vector<double> t;
			for (int i = 0; i < data.size(); i++) {
				t.push_back(data[i]-avg);
			}
			return t;
		}

		std::wstring CentralizationPreprocessor::ToString()
		{
			return L"Centralization";
		}
	}
}
