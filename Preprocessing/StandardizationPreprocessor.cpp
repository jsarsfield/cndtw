﻿#include "StandardizationPreprocessor.h"

namespace NDtw
{
	namespace Preprocessing
	{

		std::vector<double> StandardizationPreprocessor::Preprocess(std::vector<double> &data)
		{
			//http://stats.stackexchange.com/questions/1944/what-is-the-name-of-this-normalization
			//http://stats.stackexchange.com/questions/13412/what-are-the-primary-differences-between-z-scores-and-t-scores-and-are-they-bot
			//http://mathworld.wolfram.com/StandardDeviation.html

			// x = (x - mean) / std dev
			auto mean = Mean(data);

			std::vector<double> r;
			for (int i = 0; i < data.size(); i++) {
				r.push_back((data[i]-mean)*2);
			}
			auto stdDev = std::sqrt(Sum(r) / (data.size() - 1));
			r.clear();
			for (int i = 0; i < data.size(); i++) {
				r.push_back((data[i] - mean) / stdDev);
			}
			return r;
		}

		std::wstring StandardizationPreprocessor::ToString()
		{
			return L"Standardization";
		}
	}
}
