﻿#include "NormalizationPreprocessor.h"

namespace NDtw
{
	namespace Preprocessing
	{

		NormalizationPreprocessor::NormalizationPreprocessor() : NormalizationPreprocessor(0, 1)
		{
		}

		NormalizationPreprocessor::NormalizationPreprocessor(double minBoundary, double maxBoundary) : _minBoundary(minBoundary), _maxBoundary(maxBoundary)
		{
		}

		std::vector<double> NormalizationPreprocessor::Preprocess(std::vector<double> &data)
		{
			// x = ((x - min_x) / (max_x - min_x)) * (maxBoundary - minBoundary) + minBoundary

			auto min = Min(data);
			auto max = Max(data);
			auto constFactor = (_maxBoundary - _minBoundary) / (max - min);

			std::vector<double> r;
			for (int i = 0; i < data.size(); i++) {
				r.push_back((data[i] - min) * constFactor + _minBoundary);
			}
			return r;
		}

		std::wstring NormalizationPreprocessor::ToString()
		{
			return L"Normalization";
		}
	}
}
