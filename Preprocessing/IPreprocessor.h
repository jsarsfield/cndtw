﻿#pragma once

#include <string>
#include <vector>
#include "../utils.h"

namespace NDtw
{
	namespace Preprocessing
	{
		class IPreprocessor
		{
		public:
			virtual std::vector<double> Preprocess(std::vector<double> &data) = 0;
			virtual std::wstring ToString() = 0;
		};
	}
}
