﻿#pragma once

#include "IPreprocessor.h"
#include <string>
#include <vector>
#include <cmath>


namespace NDtw
{
	namespace Preprocessing
	{
		class StandardizationPreprocessor : public IPreprocessor
		{
		public:
			std::vector<double> Preprocess(std::vector<double> &data);

			virtual std::wstring ToString() override;
		};
	}
}
