﻿#pragma once

#include "IPreprocessor.h"
#include <string>
#include <vector>
#include <numeric>

namespace NDtw
{
	namespace Preprocessing
	{
		class CentralizationPreprocessor : public IPreprocessor
		{
		public:
			std::vector<double> Preprocess(std::vector<double> &data);

			virtual std::wstring ToString() override;
		};
	}
}
