/* File: example.i */
%module nDtw

%{
#include "Dtw.h"
%}
%include "std_vector.i"
%include "std_shared_ptr.i"
%include "std_list.i"
%include "std_pair.i"
%include "typemaps.i"
%include "DistanceMeasure.h"
%include <boost/optional.hpp>
%include "utils.h"
%nodefaultctor IDtw;

%typemap(in) boost::optional<int> %{
if($input == Py_None)
    $1 = boost::optional<int>();
else
    $1 = boost::optional<int>(PyLong_AsLong($input));
%}

%typemap(in) boost::optional<std::vector<std::pair< int, int> >&> %{
	$1 = boost::none;
%}

namespace std {
    %template(vectord) vector<double>;
    %template() std::pair<int,int>;
    %template(PairVector) std::vector<std::pair< int, int> >;
};

namespace NDtw
{
	enum class DistanceMeasure
	{
		Manhattan = 1,
		Euclidean = 2,
		SquaredEuclidean = 3,
		Maximum = 4
	};
}

namespace NDtw
{
	class IDtw
	{
	};
}
namespace NDtw
{
	class Dtw : public IDtw
	{
	public:
		Dtw(std::vector<double> x, std::vector<double> y, DistanceMeasure distanceMeasure, bool boundaryConstraintStart, bool boundaryConstraintEnd, boost::optional<int> slopeStepSizeDiagonal, boost::optional<int> slopeStepSizeAside, boost::optional<int> sakoeChibaMaxShift);
		double GetCost(bool bNorm);
		double PyGetCost();
		std::vector<std::pair<int, int>> GetPathNew();
		std::vector<std::pair<int, int>> GetPath();
	};
}
