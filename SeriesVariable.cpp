﻿#include "SeriesVariable.h"
#include "Preprocessing/IPreprocessor.h"

using namespace NDtw::Preprocessing;

namespace NDtw
{

	SeriesVariable::SeriesVariable(std::vector<double> &x, std::vector<double> &y, const std::wstring &variableName, IPreprocessor *preprocessor, double weight) : _x(x), _y(y), _variableName(variableName), _preprocessor(preprocessor), _weight(weight)
	{
	}

	const std::wstring SeriesVariable::getVariableName()
	{
		return _variableName;
	}

	const double SeriesVariable::getWeight()
	{
		return _weight;
	}

	const std::vector<double> SeriesVariable::getOriginalXSeries()
	{
		return _x;
	}

	const std::vector<double> SeriesVariable::getOriginalYSeries()
	{
		return _y;
	}

	std::vector<double> SeriesVariable::GetPreprocessedXSeries()
	{
		if (_preprocessor == nullptr)
		{
			return _x;
		}

		return _preprocessor->Preprocess(_x);
	}

	std::vector<double> SeriesVariable::GetPreprocessedYSeries()
	{
		if (_preprocessor == nullptr)
		{
			return _y;
		}

		return _preprocessor->Preprocess(_y);
	}
}
