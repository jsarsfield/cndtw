// Test.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "DTW.h"
#include <string> 
#include <iostream>
#include <boost\optional\optional.hpp>
using namespace std;
using namespace NDtw;

int main()
{
	cout << "Begin\n";
	vector<double> x = { 0, 1, 0 };
	vector<double> y = { 0, -1, 0.1, 0.4, 0.5, 0.4, 0.1, 0 };
	Dtw dtw(x, y, DistanceMeasure::Manhattan, false, true);
	auto path = dtw.GetPathNew();
	//cout << std::to_string(dtw.GetTotalCost()) << "\n";
	//cout << std::to_string(dtw.GetPathNew()[0].first) << " " << std::to_string(dtw.GetPathNew()[0].second) << "\n";
	cout << "\nEnd";
	int a;
	cin >> a;
	return 0;
}