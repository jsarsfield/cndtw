﻿#pragma once

#include <string>
#include <vector>

//C# TO C++ CONVERTER NOTE: Forward class declarations:
namespace NDtw { namespace Preprocessing { class IPreprocessor; } }

using namespace NDtw::Preprocessing;

namespace NDtw
{
	class SeriesVariable
	{
	private:
		std::vector<double> _x;
		std::vector<double> _y;
		const std::wstring _variableName;
		IPreprocessor *const _preprocessor;
		const double _weight;

	public:
		SeriesVariable(std::vector<double> &x, std::vector<double> &y, const std::wstring &variableName = L"", IPreprocessor *preprocessor = nullptr, double weight = 1);

		const std::wstring getVariableName();

		const double getWeight();

		const std::vector<double> getOriginalXSeries();

		const std::vector<double> getOriginalYSeries();

		std::vector<double> GetPreprocessedXSeries();

		std::vector<double> GetPreprocessedYSeries();
	};
}
