#pragma once
#include <vector>
#include <numeric>
#include <algorithm>
#include <iterator>

static double Min(std::vector<double> v) { auto it = std::min_element(v.begin(), v.end()); return *it; }
static double Max(std::vector<double> v) { auto it = std::max_element(v.begin(), v.end()); return *it; }
static double Sum(std::vector<double> v) { return std::accumulate(v.begin(), v.end(), 0.0); };
static double Mean(std::vector<double> v) { return std::accumulate(v.begin(), v.end(), 0.0) / v.size(); };
static double Std(std::vector<double> v) {
	double sum = std::accumulate(v.begin(), v.end(), 0.0);
	double mean = sum / v.size();

	std::vector<double> diff(v.size());
	std::transform(v.begin(), v.end(), diff.begin(), [mean](double x) { return x - mean; });
	double sq_sum = std::inner_product(diff.begin(), diff.end(), diff.begin(), 0.0);
	double stdev = std::sqrt(sq_sum / v.size());
	return stdev;
};

