﻿#include "Dtw.h"
#include "SeriesVariable.h"

namespace NDtw
{
	/*
	Dtw::Dtw(std::vector<double> x, std::vector<double> y, DistanceMeasure distanceMeasure, bool boundaryConstraintStart, bool boundaryConstraintEnd, boost::optional<int> slopeStepSizeDiagonal, boost::optional<int> slopeStepSizeAside, boost::optional<int> sakoeChibaMaxShift) : _xLen(1), _yLen(1), _isXLongerOrEqualThanY(true), _signalsLengthDifference(1), _seriesVariables(std::vector<SeriesVariable*>()), _distanceMeasure(DistanceMeasure::SquaredEuclidean), _boundaryConstraintStart(true), _boundaryConstraintEnd(true), _sakoeChibaConstraint(true), _sakoeChibaMaxShift(1), _useSlopeConstraint(true), _slopeMatrixLookbehind(1), _slopeStepSizeDiagonal(1), _slopeStepSizeAside(1)
	{

	}*/
	Dtw::Dtw(std::vector<double> &x, std::vector<double> &y, DistanceMeasure distanceMeasure, bool boundaryConstraintStart, bool boundaryConstraintEnd, boost::optional<int> slopeStepSizeDiagonal, boost::optional<int> slopeStepSizeAside, boost::optional<int> sakoeChibaMaxShift) //: Dtw(std::vector<SeriesVariable*>{new SeriesVariable(x, y)}, distanceMeasure, boundaryConstraintStart, boundaryConstraintEnd, slopeStepSizeDiagonal, slopeStepSizeAside, sakoeChibaMaxShift)
	{
		this->x = x;
		this->y = y;
	}

	Dtw::Dtw(std::vector<SeriesVariable*> &seriesVariables, DistanceMeasure distanceMeasure, bool boundaryConstraintStart, bool boundaryConstraintEnd, boost::optional<int> slopeStepSizeDiagonal, boost::optional<int> slopeStepSizeAside, boost::optional<int> sakoeChibaMaxShift)
	{

		_seriesVariables = seriesVariables;
		_distanceMeasure = distanceMeasure;
		_boundaryConstraintStart = boundaryConstraintStart;
		_boundaryConstraintEnd = boundaryConstraintEnd; 
		
		if (seriesVariables.empty() || seriesVariables.empty())
		{
			//ArgumentException tempVar(L"Series should have values for at least one variable.");
			//throw &tempVar;
		}

		for (int i = 1; i < _seriesVariables.size(); i++)
		{
			if (_seriesVariables[i]->getOriginalXSeries().size() != _seriesVariables[0]->getOriginalXSeries().size())
			{
				//ArgumentException tempVar2(L"All variables withing series should have the same number of values.");
				//throw &tempVar2;
			}

			if (_seriesVariables[i]->getOriginalYSeries().size() != _seriesVariables[0]->getOriginalYSeries().size())
			{
				//ArgumentException tempVar3(L"All variables withing series should have the same number of values.");
				//throw &tempVar3;
			}
		}

		_xLen = _seriesVariables[0]->getOriginalXSeries().size(); 
		_yLen = _seriesVariables[0]->getOriginalYSeries().size();
		if (_xLen == 0 || _yLen == 0)
		{
			//ArgumentException tempVar4(L"Both series should have at least one value.");
			//throw &tempVar4;
		}

		if (sakoeChibaMaxShift && sakoeChibaMaxShift < 0)
		{
			//ArgumentException tempVar5(L"Sakoe-Chiba max shift value should be positive or null.");
			//throw &tempVar5;
		}

		_isXLongerOrEqualThanY = _xLen >= _yLen;
		_signalsLengthDifference = std::abs(_xLen - _yLen);
		_sakoeChibaConstraint = sakoeChibaMaxShift.get();
		if (sakoeChibaMaxShift != boost::none)
			_sakoeChibaMaxShift = sakoeChibaMaxShift.get();
		else
			_sakoeChibaMaxShift = std::numeric_limits<int>::max();
		if (slopeStepSizeAside || slopeStepSizeDiagonal)
		{
			if (!slopeStepSizeAside || !slopeStepSizeDiagonal)
			{
				//ArgumentException tempVar6(L"Both values or none for slope constraint must be specified.");
				//throw &tempVar6;
			}

			if (slopeStepSizeDiagonal < 1)
			{
				//ArgumentException tempVar7(L"Diagonal slope constraint parameter must be greater than 0.");
				//throw &tempVar7;
			}

			if (slopeStepSizeAside < 0)
			{
				//ArgumentException tempVar8(L"Diagonal slope constraint parameter must be greater or equal to 0.");
				//throw &tempVar8;
			}

			_useSlopeConstraint = true;
			_slopeStepSizeAside = slopeStepSizeAside.get();
			_slopeStepSizeDiagonal = slopeStepSizeDiagonal.get();
			_slopeMatrixLookbehind = slopeStepSizeDiagonal.get() + slopeStepSizeAside.get();	
		}

		//todo: throw error when solution (path from (1, 1) to (m, n) is not even possible due to slope constraints)
	}

	void Dtw::InitializeArrays()
	{
		_distances = std::vector<std::vector<double>>(_xLen + _slopeMatrixLookbehind);
		for (int i = 0; i < _xLen + _slopeMatrixLookbehind; i++)
		{
			_distances[i] = std::vector<double>(_yLen + _slopeMatrixLookbehind);
		}

		_pathCost = std::vector<std::vector<double>>(_xLen + _slopeMatrixLookbehind);
		for (int i = 0; i < _xLen + _slopeMatrixLookbehind; i++)
		{
			_pathCost[i] = std::vector<double>(_yLen + _slopeMatrixLookbehind);
		}

		_predecessorStepX = std::vector<std::vector<std::vector<int>>>(_xLen + _slopeMatrixLookbehind);
		for (int i = 0; i < _xLen + _slopeMatrixLookbehind; i++)
		{
			_predecessorStepX[i] = std::vector<std::vector<int>>(_yLen + _slopeMatrixLookbehind);
		}

		_predecessorStepY = std::vector<std::vector<std::vector<int>>>(_xLen + _slopeMatrixLookbehind);
		for (int i = 0; i < _xLen + _slopeMatrixLookbehind; i++)
		{
			_predecessorStepY[i] = std::vector<std::vector<int>>(_yLen + _slopeMatrixLookbehind);
		}
	}

	void Dtw::CalculateDistances()
	{
		for (int additionalIndex = 1; additionalIndex <= _slopeMatrixLookbehind; additionalIndex++)
		{
			//initialize [x.len - 1 + additionalIndex][all] elements
			for (int i = 0; i < _yLen + _slopeMatrixLookbehind; i++)
			{
				_pathCost[_xLen - 1 + additionalIndex][i] = std::numeric_limits<double>::infinity();
			}

			//initialize [all][y.len - 1 + additionalIndex] elements
			for (int i = 0; i < _xLen + _slopeMatrixLookbehind; i++)
			{
				_pathCost[i][_yLen - 1 + additionalIndex] = std::numeric_limits<double>::infinity();
			}
		}

		//calculate distances for 'data' part of the matrix
		for (auto seriesVariable : _seriesVariables)
		{
			auto xSeriesForVariable = seriesVariable->GetPreprocessedXSeries();
			auto ySeriesForVariable = seriesVariable->GetPreprocessedYSeries();
			//weight for current variable distances that is applied BEFORE the value is further transformed by distance measure
			auto variableWeight = seriesVariable->getWeight();

			for (int i = 0; i < _xLen; i++)
			{
				auto& currentDistances = _distances[i];
				auto xVal = xSeriesForVariable[i];
				for (int j = 0; j < _yLen; j++)
				{
					if (_distanceMeasure == DistanceMeasure::Manhattan)
					{
						currentDistances[j] += std::abs(xVal - ySeriesForVariable[j]) * variableWeight;
					}
					else if (_distanceMeasure == DistanceMeasure::Maximum)
					{
						currentDistances[j] = std::max(currentDistances[j], std::abs(xVal - ySeriesForVariable[j]) * variableWeight);
					}
					else
					{
						//Math.Pow(xVal - ySeriesForVariable[j], 2) is much slower, so direct multiplication with temporary variable is used
						auto dist = (xVal - ySeriesForVariable[j]) * variableWeight;
						currentDistances[j] += dist * dist;
					}
				}
			}
		}

		if (_distanceMeasure == DistanceMeasure::Euclidean)
		{
			for (int i = 0; i < _xLen; i++)
			{
				auto& currentDistances = _distances[i];
				for (int j = 0; j < _yLen; j++)
				{
					currentDistances[j] = std::sqrt(currentDistances[j]);
				}
			}
		}
	}

	void Dtw::CalculateWithoutSlopeConstraint()
	{
		auto stepMove0 = std::vector<int> {0};
		auto stepMove1 = std::vector<int> {1};

		for (int i = _xLen - 1; i >= 0; i--)
		{
			auto& currentRowDistances = _distances[i];
			auto& currentRowPathCost = _pathCost[i];
			auto& previousRowPathCost = _pathCost[i + 1];

			auto& currentRowPredecessorStepX = _predecessorStepX[i];
			auto& currentRowPredecessorStepY = _predecessorStepY[i];

			for (int j = _yLen - 1; j >= 0; j--)
			{
				//Sakoe-Chiba constraint, but make it wider in one dimension when signal lengths are not equal
				if (_sakoeChibaConstraint && (_isXLongerOrEqualThanY ? j > i && j - i > _sakoeChibaMaxShift || j < i && i - j > _sakoeChibaMaxShift + _signalsLengthDifference : j > i && j - i > _sakoeChibaMaxShift + _signalsLengthDifference || j < i && i - j > _sakoeChibaMaxShift))
				{
					currentRowPathCost[j] = std::numeric_limits<double>::infinity();
					continue;
				}

				auto diagonalNeighbourCost = previousRowPathCost[j + 1];
				auto xNeighbourCost = previousRowPathCost[j];
				auto yNeighbourCost = currentRowPathCost[j + 1];

				//on the topright edge, when boundary constrained only assign current distance as path distance to the (m, n) element
				//on the topright edge, when not boundary constrained, assign current distance as path distance to all edge elements
				if (std::isinf(diagonalNeighbourCost) && (!_boundaryConstraintEnd || i - j == _xLen - _yLen))
				{
					currentRowPathCost[j] = currentRowDistances[j];
				}
				else if (diagonalNeighbourCost <= xNeighbourCost && diagonalNeighbourCost <= yNeighbourCost)
				{
					currentRowPathCost[j] = diagonalNeighbourCost + currentRowDistances[j];
					currentRowPredecessorStepX[j] = stepMove1;
					currentRowPredecessorStepY[j] = stepMove1;
				}
				else if (xNeighbourCost <= yNeighbourCost)
				{
					currentRowPathCost[j] = xNeighbourCost + currentRowDistances[j];
					currentRowPredecessorStepX[j] = stepMove1;
					currentRowPredecessorStepY[j] = stepMove0;
				}
				else
				{
					currentRowPathCost[j] = yNeighbourCost + currentRowDistances[j];
					currentRowPredecessorStepX[j] = stepMove0;
					currentRowPredecessorStepY[j] = stepMove1;
				}
			}
		}
	}

	void Dtw::CalculateWithSlopeLimit()
	{
		//precreated array that contain arrays of steps which are used when stepaside path is the optimal one
		//stepAsideMoves*[0] is empty element, because contents are 1-based, access to elements is faster that way
		auto stepAsideMovesHorizontalX = std::vector<std::vector<int>>(_slopeStepSizeAside + 1);
		auto stepAsideMovesHorizontalY = std::vector<std::vector<int>>(_slopeStepSizeAside + 1);
		auto stepAsideMovesVerticalX = std::vector<std::vector<int>>(_slopeStepSizeAside + 1);
		auto stepAsideMovesVerticalY = std::vector<std::vector<int>>(_slopeStepSizeAside + 1);
		for (int i = 1; i <= _slopeStepSizeAside; i++)
		{
			auto movesXHorizontal = std::vector<int>();
			auto movesYHorizontal = std::vector<int>();
			auto movesXVertical = std::vector<int>();
			auto movesYVertical = std::vector<int>();

			//make steps in horizontal/vertical direction
			for (int stepAside = 1; stepAside <= i; stepAside++)
			{
				movesXHorizontal.push_back(1);
				movesYHorizontal.push_back(0);

				movesXVertical.push_back(0);
				movesYVertical.push_back(1);
			}

			//make steps in diagonal direction
			for (int stepForward = 1; stepForward <= _slopeStepSizeDiagonal; stepForward++)
			{
				movesXHorizontal.push_back(1);
				movesYHorizontal.push_back(1);

				movesXVertical.push_back(1);
				movesYVertical.push_back(1);
			}

			stepAsideMovesHorizontalX[i] = movesXHorizontal;
			stepAsideMovesHorizontalY[i] = movesYHorizontal;

			stepAsideMovesVerticalX[i] = movesXVertical;
			stepAsideMovesVerticalY[i] = movesYVertical;
		}

		auto stepMove1 = std::vector<int> {1};

		for (int i = _xLen - 1; i >= 0; i--)
		{
			auto& currentRowDistances = _distances[i];

			auto& currentRowPathCost = _pathCost[i];
			auto& previousRowPathCost = _pathCost[i + 1];

			auto& currentRowPredecessorStepX = _predecessorStepX[i];
			auto& currentRowPredecessorStepY = _predecessorStepY[i];

			for (int j = _yLen - 1; j >= 0; j--)
			{
				//Sakoe-Chiba constraint, but make it wider in one dimension when signal lengths are not equal
				if (_sakoeChibaConstraint && (_isXLongerOrEqualThanY ? j > i && j - i > _sakoeChibaMaxShift || j < i && i - j > _sakoeChibaMaxShift + _signalsLengthDifference : j > i && j - i > _sakoeChibaMaxShift + _signalsLengthDifference || j < i && i - j > _sakoeChibaMaxShift))
				{
					currentRowPathCost[j] = std::numeric_limits<double>::infinity();
					continue;
				}

				//just initialize lowest cost with diagonal neighbour element
				auto lowestCost = previousRowPathCost[j + 1];
				auto lowestCostStepX = stepMove1;
				auto lowestCostStepY = stepMove1;

				for (int alternativePathAside = 1; alternativePathAside <= _slopeStepSizeAside; alternativePathAside++)
				{
					auto costHorizontalStepAside = 0.0;
					auto costVerticalStepAside = 0.0;

					for (int stepAside = 1; stepAside <= alternativePathAside; stepAside++)
					{
						costHorizontalStepAside += _distances[i + stepAside][j];
						costVerticalStepAside += _distances[i][j + stepAside];
					}

					for (int stepForward = 1; stepForward < _slopeStepSizeDiagonal; stepForward++)
					{
						costHorizontalStepAside += _distances[i + alternativePathAside + stepForward][j + stepForward];
						costVerticalStepAside += _distances[i + stepForward][j + alternativePathAside + stepForward];
					}

					//at final step, add comulative cost
					costHorizontalStepAside += _pathCost[i + alternativePathAside + _slopeStepSizeDiagonal][j + _slopeStepSizeDiagonal];

					//at final step, add comulative cost
					costVerticalStepAside += _pathCost[i + _slopeStepSizeDiagonal][j + alternativePathAside + _slopeStepSizeDiagonal];

					//check if currently considered horizontal stepaside is better than the best option found until now
					if (costHorizontalStepAside < lowestCost)
					{
						lowestCost = costHorizontalStepAside;
						lowestCostStepX = stepAsideMovesHorizontalX[alternativePathAside];
						lowestCostStepY = stepAsideMovesHorizontalY[alternativePathAside];
					}

					//check if currently considered vertical stepaside is better than the best option found until now
					if (costVerticalStepAside < lowestCost)
					{
						lowestCost = costVerticalStepAside;
						lowestCostStepX = stepAsideMovesVerticalX[alternativePathAside];
						lowestCostStepY = stepAsideMovesVerticalY[alternativePathAside];
					}
				}

				//on the topright edge, when boundary constrained only assign current distance as path distance to the (m, n) element
				//on the topright edge, when not boundary constrained, assign current distance as path distance to all edge elements
				if (std::isinf(lowestCost) && (!_boundaryConstraintEnd || i - j == _xLen - _yLen))
				{
					lowestCost = 0;
				}

				currentRowPathCost[j] = lowestCost + currentRowDistances[j];
				currentRowPredecessorStepX[j] = lowestCostStepX;
				currentRowPredecessorStepY[j] = lowestCostStepY;
			}
		}
	}

	void Dtw::Calculate()
	{
		if (!_calculated)
		{
			InitializeArrays();
			CalculateDistances();

			if (_useSlopeConstraint)
			{
				CalculateWithSlopeLimit();
			}
			else
			{
				CalculateWithoutSlopeConstraint();
			}

			_calculated = true;
		}
	}

	double Dtw::GetCost(bool bNorm)
	{
		Calculate();

		if (_boundaryConstraintStart)
		{
			if (bNorm)
				return _pathCost[0][0] / sqrt(getXLength() * getXLength() + getYLength() * getYLength());
			else
				return _pathCost[0][0];
		}
		std::vector<double> y;
		for (int i = 0; i < _pathCost.size(); i++) {
			y.push_back(_pathCost[i][0]);
		}
		if (bNorm)
			return std::min(Min(_pathCost[0]), Min(y)) / sqrt(getXLength() * getXLength() + getYLength() * getYLength());
		else
			return std::min(Min(_pathCost[0]), Min(y));
	}

	double Dtw::PyGetCost() {
		return this->pathOptCost;
	}

	double Dtw::GetTotalCost(boost::optional<std::vector<std::pair<int, int>>&> pathRef)
	{
		std::vector<std::pair<int, int>>& path = (pathRef) ? *pathRef : this->pathOpt;
		auto sv = this->getSeriesVariables()[0];
		auto& x = sv->getOriginalXSeries(); // Ground truth signal
		auto& y = sv->getOriginalYSeries(); // Patient signal
		double total_cost = 0; // Store total cost
		int threshold = 10; // Threshold percentage 

		// Get abs diff between min max of GT & path signal if abs diff of path is greater than threshold % of GT then continue costing else return infinity cost

		// z-normalise GT & path
		
		/********** START OLD CODE UNCOMMENT IF NEEDED********************
		if (path[0].first != 0) {
			return 100;  // Large cost returned when first element in ground truth is not on warping path, this occurs when patient signal is small. Consider returning infinity
		}
		int gt_index = 0; // Current index of ground truth signal we are evaluating
		int i = 0; // Index of path we are evaluating
		// Loop remaining elements of ground truth to calcualte cost of closest patient data point to each ground truth data point
		while(gt_index < x.size()) {
			std::vector<double> y_vals; // Y indices that are aligned to current gt_index
			// Calculate absolute difference
			while(i < path.size() && path[i].first == gt_index){
				y_vals.push_back(std::abs(x[gt_index]-y[path[i].second]));
				i++;
			}
			// Get minimum difference
			double min = *std::min_element(std::begin(y_vals), std::end(y_vals));
			total_cost += min;
			gt_index++;
		}
		************ END OLD CODE UNCOMMENT IF NEEDED********************/
		return total_cost;
	}

	double Dtw::Dist(const double &a, const double &b) {
		return std::abs(a - b);
	}

	const std::pair<const int&,const double&> Dtw::MinNew(const double &a, const double &b, const double &c) {
		const double vals[] = {a,b,c };
		int it = std::distance(std::begin(vals),std::min_element(std::begin(vals), std::end(vals)));
		return std::make_pair(it, vals[it]);
	}

	void Dtw::Znorm(std::vector<double>& v) {
		double sum = std::accumulate(std::begin(v), std::end(v), 0.0);
		double mean = sum / v.size();

		double accum = 0.0;
		std::for_each(std::begin(v), std::end(v), [&](const double d) {
			accum += (d - mean) * (d - mean);
		});

		double std_dev = sqrt(accum / v.size());

		std::for_each(std::begin(v), std::end(v), [&](double& d) {
			d = (d - mean) / std_dev;
		});
	}

	double Dtw::OnlineZnorm(const double& v, const double& mean, const double& std_dev) {
		return (v - mean) / std_dev;
	}

	std::vector<std::pair<int, int>>& Dtw::GetPathNew()
	{
		//auto sv = this->getSeriesVariables()[0]; // Get series variable
		auto x = this->x; // Ground truth signal
		auto y = this->y; // Patient signal
		paths = icube(x.size(), y.size(), 2); // Stores indices of all paths, contians indices of neighbour with smallest cost, used to find the optimal warping path. 3rd dimension 0=row 1=col
		costs = mat(x.size(), y.size()); // Costs matrix stores summed cost starting from end of both signals with cost of zero 
		xLast = x.size() - 1; // Index of last element
		yLast = y.size() - 1; // Index of last element
		int row = 0; // Index of row we are costing, starts at zero
		int col = yLast; // Index of col we are costing
		int lowestCumCol = yLast; // Index of column with lowest cumulative total on row zero
		// Populate matrices with -1 to show they have not yet been written too
		paths.fill(-1);
		costs.fill(-1);
		double best = INF; // Store best cost of subsequence
		double r_mean = 0; // Running std for calculating online z-normalization
		double r_std = 0; // Running mean for calculating online z-normalization

		// Index we are costing is top right corner i.e. (xLast,yLast)
		//costs(row, col) = Dist(x[row], y[col]);
		//row = row-1; // As we have calculated top right take one off row

		// Get 10% threshold of abs diff minmax of GT
		auto r = std::minmax_element(this->x.begin(), this->x.end());
		double threshold = std::abs(r.first -r.second)*0.1;
		// z-norm GT signal
		Znorm(x);

		// Loop columns from right to left
		for (col; col >= 0; col--) {
			r_mean += y[col];
			r_std += y[col] * y[col];
			auto r = std::minmax_element(this->y.begin()+col, this->y.end());
			if (std::abs(r.first - r.second) < threshold) // If unnormalised subsequence has an abs minmax lower than threshold then skip this subsequence
				continue;
			int scol = col; // Store subsequence col
			double dist = 0; // Current cost/distanec of subsequence path
			std::vector<std::pair<int, int>> subpath;
			int sub_size = y.size() - col; // Size of subsequence
			double mean = r_mean / sub_size;
			double std_dev = sqrt((r_std / sub_size) - (mean*mean));

			// Find path & cost subsequence from this col
			while (row <= xLast && scol <= yLast && dist < best) { // While there are still cells to evalute and this subsequence dist is less than best
				// Add to path
				subpath.push_back(std::make_pair(row, scol));
				if (scol != yLast && row != xLast) { // Index we are costing is not on topmost row or rightmost column
					double ynp = OnlineZnorm(y[scol + 1], mean, std_dev);
					auto res = MinNew(std::abs(x[row+1] - ynp), std::abs(x[row] - ynp), std::abs(x[row + 1] - OnlineZnorm(y[scol], mean, std_dev)));
					dist += res.second;
					if (res.first == 0) { // row + 1, col + 1
						scol += 1;
						row += 1;
					}
					else if (res.first == 1) { // row, col + 1
						scol += 1;
					}
					else { // row + 1, col
						row += 1;
					}
				}
				else if (row == xLast) { // Index we are costing is on topmost row
					dist += std::abs(x[row] - OnlineZnorm(y[scol],mean,std_dev));
					scol += 1;
				}
				else { // Index we are costing is on rightmost column
					dist += std::abs(x[row] - OnlineZnorm(y[scol], mean, std_dev));
					row += 1;
				}
			}
			row = 0;
			// Update if best
			if (dist < best) {
				best = dist;
				this->pathOpt = subpath;
			}
			/*
			// Loop rows from top to bottom
			for (row; row >= 0; row--) {


				if (col != yLast && row != xLast) { // Index we are costing is not on top row or right column
					auto res = MinNew(costs(row + 1, col + 1), costs(row, col + 1), costs(row + 1, col));
					costs(row, col) = Dist(x[row], y[col]) + res.second;
					// Determine neighbour with lowest cost for paths var
					if (res.first == 0) { // row + 1, col + 1
						paths(row, col, 0) = row + 1;
						paths(row, col, 1) = col + 1;
					}
					else if (res.first == 1) { // row, col + 1
						paths(row, col, 0) = row;
						paths(row, col, 1) = col + 1;
					}
					else { // row + 1, col
						paths(row, col, 0) = row + 1;
						paths(row, col, 1) = col;
					}
				}
				else if (row == xLast) { // Index we are costing is on top row
					costs(row, col) = Dist(x[row], y[col]) + costs(row, col + 1);
					paths(row, col, 0) = row;
					paths(row, col, 1) = col + 1;
				}
				else if (col == yLast) { // Index we are costing is on right column
					costs(row, col) = Dist(x[row], y[col]) + costs(row + 1, col);
					paths(row, col, 0) = row + 1;
					paths(row, col, 1) = col;
				}

				if (row == 0) { // Cost path/subsequence to see if smaller cost to current pathOpt
					if (col != yLast) { // If not rightmost column
						std::vector<std::pair<int, int>> pathNew;
						GetPathFrom(pathNew, row, col);
						double pathNewCost = GetTotalCost(pathNew);
						if (pathNewCost < pathOptCost) { // If cost of new path is lower then update object pathOpt var
							this->pathOpt = pathNew;
							this->pathOptCost = pathNewCost;
						}
					}
					else {
						 GetPathFrom(this->pathOpt, row, yLast);
						 this->pathOptCost = GetTotalCost(this->pathOpt);

					}
				}
				
			}
			row = xLast; // Set row back to top
			*/
		}
		// Find optimal path start index
		/*
		while (true) {
			if (lowestCumCol == 0) { // If column is zero then break as we have start index
				break;
			}
			else if (std::abs(costs(0, lowestCumCol) - costs(0, lowestCumCol - 1)) >= std::abs(costs(0, lowestCumCol) - costs(paths(0, lowestCumCol, 0), paths(0, lowestCumCol, 1)))) { // If the difference between lowestCumCol and cell to left is >= than difference between lowestCumCol and min neighbour then use lowestCumCol as start index
				break;
			}
			lowestCumCol--; // Continue search for start index
		}*/
		// Find optimal path
		//this->pathOpt = GetPathFrom(0,lowestCumCol);
		
		return this->pathOpt;
	}

	void Dtw::GetPathFrom(std::vector<std::pair<int, int>>& path, int row, int col) {
		path.clear();
		path.push_back(std::make_pair(row, col)); // Add start path
		while (true) { // While index of last elements in signals are not on path
			int rowt = paths(row, col, 0); // rowt temp var
			col = paths(row, col, 1);
			row = rowt;
			path.push_back(std::make_pair(row, col));
			if (paths(row, col, 0) == xLast && paths(row, col, 1) == yLast) { // If we reached last index of signals on path then break
				path.push_back(std::make_pair(xLast, yLast)); // End boundary constraint ensures end of both signals are always aligned
				break;
			}
		}
	}

	std::vector<std::pair<int, int>> Dtw::GetPath()
	{
		Calculate();

		auto path = std::vector<std::pair<int, int>>();
		auto indexX = 0;
		auto indexY = 0;
		if (!_boundaryConstraintStart)
		{
			//find the starting element with lowest cost
			auto min = std::numeric_limits<double>::infinity();
			for (int i = 0; i < std::max(_xLen, _yLen); i++)
			{
				if (i < _xLen && _pathCost[i][0] < min)
				{
					indexX = i;
					indexY = 0;
					min = _pathCost[i][0];
				}
				if (i < _yLen && _pathCost[0][i] < min)
				{
					indexX = 0;
					indexY = i;
					min = _pathCost[0][i];
				}
			}
		}

		path.push_back(std::pair<int, int>(indexX, indexY));
		while (_boundaryConstraintEnd ? (indexX < _xLen - 1 || indexY < _yLen - 1) : (indexX < _xLen - 1 && indexY < _yLen - 1))
		{
			auto& stepX = _predecessorStepX[indexX][indexY];
			auto& stepY = _predecessorStepY[indexX][indexY];

			for (int i = 0; i < stepX.size(); i++)
			{
				indexX += stepX[i];
				indexY += stepY[i];
				path.push_back(std::pair<int, int>(indexX, indexY));
			}
		}
		return path;
	}

	std::vector<std::vector<double>> Dtw::GetDistanceMatrix()
	{
		Calculate();
		return _distances;
	}

	std::vector<std::vector<double>> Dtw::GetCostMatrix()
	{
		Calculate();
		return _pathCost;
	}

	const int &Dtw::getXLength() const
	{
		return _xLen;
	}

	const int &Dtw::getYLength() const
	{
		return _yLen;
	}

	std::vector<SeriesVariable*> Dtw::getSeriesVariables() const
	{
		return _seriesVariables;
	}

	Dtw::~Dtw() 
	{
		for (int i = 0; i < _seriesVariables.size(); i++) {
			delete _seriesVariables[i];
		}
	}
}
