﻿#pragma once

namespace NDtw
{
	enum class DistanceMeasure
	{
		Manhattan = 1,
		Euclidean = 2,
		SquaredEuclidean = 3,
		Maximum = 4
	};
}
