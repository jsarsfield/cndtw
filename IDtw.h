﻿#pragma once

#include <vector>
#include <tuple>

//C# TO C++ CONVERTER NOTE: Forward class declarations:
namespace NDtw { class SeriesVariable; }


namespace NDtw
{
	class IDtw
	{
	public:
		virtual double GetCost(bool bNorm) = 0;
		virtual std::vector<std::pair<int, int>> GetPath() = 0;
		virtual std::vector<std::vector<double>> GetDistanceMatrix() = 0;
		virtual std::vector<std::vector<double>> GetCostMatrix() = 0;
		virtual const int &getXLength() const = 0;
		virtual const int &getYLength() const = 0;
		virtual std::vector<SeriesVariable*> getSeriesVariables() const = 0;
	};
}
