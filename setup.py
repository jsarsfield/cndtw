#!/usr/bin/env python

"""
setup.py file for SWIG example
"""

from distutils.core import setup, Extension


nDtw_module = Extension('_nDtw',
                           sources=['Dtw_wrap.cxx','Dtw.cpp','utils.cpp','IDtw.cpp','Preprocessing/StandardizationPreprocessor.cpp','Preprocessing/NormalizationPreprocessor.cpp','SeriesVariable.cpp','Preprocessing/CentralizationPreprocessor.cpp','Preprocessing/NonePreprocessor.cpp','Preprocessing/IPreprocessor.cpp','DistanceMeasure.cpp'],
                           )

setup (name = 'nDtw',
       version = '0.1',
       author      = "SWIG Docs",
       description = """Simple swig example from docs""",
       ext_modules = [nDtw_module],
       py_modules = ["nDtw"],
       )