﻿#pragma once

#include "IDtw.h"
#include "DistanceMeasure.h"
#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
#include <tuple>
#include <boost/optional.hpp>
#include "utils.h"
#include <armadillo>

#define INF 1e20       //Pseudo Infitinte number for this code

//C# TO C++ CONVERTER NOTE: Forward class declarations:
namespace NDtw { class SeriesVariable; }
using namespace arma;
//(The MIT License)

//Copyright (c) 2012 Darjan Oblak

//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the 'Software'), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:

//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.

//THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.

namespace NDtw
{
	class __declspec(dllexport) Dtw : public IDtw
	{
	private:
		int _xLen;
		int _yLen;
		bool _isXLongerOrEqualThanY;
		int _signalsLengthDifference;
		std::vector<SeriesVariable*> _seriesVariables;
		DistanceMeasure _distanceMeasure;
		bool _boundaryConstraintStart;
		bool _boundaryConstraintEnd;
		bool _sakoeChibaConstraint;
		int _sakoeChibaMaxShift;
		bool _calculated = false;
		std::vector<std::vector<double>> _distances;
		std::vector<std::vector<double>> _pathCost;
		bool _useSlopeConstraint;
		int _slopeMatrixLookbehind = 1;
		int _slopeStepSizeDiagonal;
		int _slopeStepSizeAside;

		//[indexX][indexY][step]
		std::vector<std::vector<std::vector<int>>> _predecessorStepX;
		std::vector<std::vector<std::vector<int>>> _predecessorStepY;

		/// <summary>
		/// Initialize class that performs single variable DTW calculation for given series and settings.
		/// </summary>
		/// <param name="x">Series A, array of values.</param>
		/// <param name="y">Series B, array of values.</param>
		/// /// <param name="distanceMeasure">Distance measure used (how distance for value pair (p,q) of signal elements is calculated from multiple variables).</param>
		/// <param name="boundaryConstraintStart">Apply boundary constraint at (1, 1).</param>
		/// <param name="boundaryConstraintEnd">Apply boundary constraint at (m, n).</param>
		/// <param name="slopeStepSizeDiagonal">Diagonal steps in local window for calculation. Results in Ikatura paralelogram shaped dtw-candidate space. Use in combination with slopeStepSizeAside parameter. Leave null for no constraint.</param>
		/// <param name="slopeStepSizeAside">Side steps in local window for calculation. Results in Ikatura paralelogram shaped dtw-candidate space. Use in combination with slopeStepSizeDiagonal parameter. Leave null for no constraint.</param>
		/// <param name="sakoeChibaMaxShift">Sakoe-Chiba max shift constraint (side steps). Leave null for no constraint.</param>
	public:

		/// <summary>
		/// Initialize class that performs multivariate DTW calculation for given series and settings.
		/// </summary>
		/// <param name="seriesVariables">Array of series value pairs for different variables with additional options for data preprocessing and weights.</param>
		/// <param name="distanceMeasure">Distance measure used (how distance for value pair (p,q) of signal elements is calculated from multiple variables).</param>
		/// <param name="boundaryConstraintStart">Apply boundary constraint at (1, 1).</param>
		/// <param name="boundaryConstraintEnd">Apply boundary constraint at (m, n).</param>
		/// <param name="slopeStepSizeDiagonal">Diagonal steps in local window for calculation. Results in Ikatura paralelogram shaped dtw-candidate space. Use in combination with slopeStepSizeAside parameter. Leave null for no constraint.</param>
		/// <param name="slopeStepSizeAside">Side steps in local window for calculation. Results in Ikatura paralelogram shaped dtw-candidate space. Use in combination with slopeStepSizeDiagonal parameter. Leave null for no constraint.</param>
		/// <param name="sakoeChibaMaxShift">Sakoe-Chiba max shift constraint (side steps). Leave null for no constraint.</param>
		Dtw(std::vector<SeriesVariable*> &seriesVariables, DistanceMeasure distanceMeasure = DistanceMeasure::Manhattan, bool boundaryConstraintStart = true, bool boundaryConstraintEnd = true, boost::optional<int> slopeStepSizeDiagonal = boost::none, boost::optional<int> slopeStepSizeAside = boost::none, boost::optional<int> sakoeChibaMaxShift = boost::none);
		//Dtw(std::vector<double> x, std::vector<double> y, DistanceMeasure distanceMeasure, bool boundaryConstraintStart, bool boundaryConstraintEnd, boost::optional<int> slopeStepSizeDiagonal = boost::none, boost::optional<int> slopeStepSizeAside = boost::none, boost::optional<int> sakoeChibaMaxShift = boost::none, int a); // temp fix int a, ensures no ambiguity with pointer constructor when build.bat
		Dtw(std::vector<double> &x, std::vector<double> &y, DistanceMeasure distanceMeasure, bool boundaryConstraintStart, bool boundaryConstraintEnd, boost::optional<int> slopeStepSizeDiagonal = 1, boost::optional<int> slopeStepSizeAside = 1, boost::optional<int> sakoeChibaMaxShift = 1);
		~Dtw();
	private:
		void InitializeArrays();

		void CalculateDistances();

		void CalculateWithoutSlopeConstraint();

		void CalculateWithSlopeLimit();

		void Calculate();

	public:
		std::vector<double> x; // GT signal
		std::vector<double> y; // Kinect stream signal
		double GetCost(bool bNorm);
		void Znorm(std::vector<double>& vec); // z-normalise vector, used to znorm ground truth signal NOT FOR ONLINE Z-NORM this is done within GetPathNew loop
		double OnlineZnorm(const double& v, const double& mean, const double& std_dev); // Online z-norm
		double GetTotalCost(boost::optional<std::vector<std::pair<int, int>>&> pathRef); // Get total cost of DTW alignment. Modified to work with signals that have been downsampled.
		double PyGetCost(); // Get pathOptCost *python interface*
		std::vector<std::pair<int, int>>& Dtw::GetPathNew(); // New method for calculating DTW
		std::vector<std::pair<int, int>> pathOpt; // Store optimal path
		double pathOptCost; // The cost of the optimal path
		void GetPathFrom(std::vector<std::pair<int, int>>& path, int row, int col); // Get path from supplied row & col
		int xLast; // Index of last element
		int yLast; // Index of last element
		icube paths; // Stores indices of all paths, contians indices of neighbour with smallest cost, used to find the optimal warping path. 3rd dimension 0=row 1=col
		mat costs; // Costs matrix stores summed cost starting from end of both signals with cost of zero 

		double Dist(const double& a, const double& b); // Distance measure absolute/manhatten

		const std::pair<const int&, const double&> MinNew(const double &i, const double &j, const double &k); // Index of min/lowest value

		std::vector<std::pair<int, int>> GetPath();

		std::vector<std::vector<double>> GetDistanceMatrix();

		std::vector<std::vector<double>> GetCostMatrix();

		const int &getXLength() const;

		const int &getYLength() const;

		std::vector<SeriesVariable*> getSeriesVariables() const;
	};
}
